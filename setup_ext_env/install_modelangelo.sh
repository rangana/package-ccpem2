while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "Make sure you have conda installed"
      echo "Make sure you have set the TORCH_HOME environment variable to a suitable public location (if installing on a cluster)"
      echo "-h, --help                   simple help and instructions"
      echo "-w, --download-weights       use if you want to also download the weights"
      exit 0
      ;;
    -w|--download-weights)
      echo "Downloading weights as well because flag -w or --download-weights was specified"
      DOWNLOAD_WEIGHTS=1
      shift
      ;;
  esac
done

if [ -z "${TORCH_HOME}" ] && [ -n "${DOWNLOAD_WEIGHTS}" ]; then
  echo "ERROR: TORCH_HOME is not set, but --download-weights or -w flag is set";
  echo "Please specify TORCH_HOME to a publicly available directory";
  exit 1;
fi

if which micromamba > /dev/null; then
  conda_type="micromamba"
  eval "$(micromamba shell hook --shell bash)"
elif which conda > /dev/null; then
  conda_type="conda"
  eval "$(conda init bash)"
fi

if ! { $conda_type env list | grep $e; } >/dev/null 2>&1; then
  $conda_type create -n model_angelo python=3.10 -c conda-forge -y;
fi

torch_home_path="${TORCH_HOME}"
# activate env
$conda_type activate model_angelo


grep_env="$($conda_type env list | grep model_angelo)"
# Check to make sure model_angelo is activated
if [[ ! $grep_env == *"*"* ]]
then
  echo "Could not run conda activate model_angelo, please check the errors";
  exit 1;
fi

$conda_type install pytorch torchvision torchaudio -c pytorch -c nvidia -c conda-forge -y

if which nvidia-smi > /dev/null; then
  $conda_type install pytorch-cuda=11.7 -c nvidia -c pytorch -y
fi

if [ "${torch_home_path}" ]
then
  $conda_type env config vars set TORCH_HOME="${torch_home_path}"
fi

python_exc="${CONDA_PREFIX}/bin/python"

$python_exc -mpip install -r requirements.txt
$python_exc setup.py install

if [[ "${DOWNLOAD_WEIGHTS}" ]]; then
  echo "Writing weights to ${TORCH_HOME}"
  $python_exc model_angelo/utils/setup_weights.py --bundle-name nucleotides
  $python_exc model_angelo/utils/setup_weights.py --bundle-name nucleotides_no_seq
else
  echo "Did not download weights because the flag -w or --download-weights was not specified"
fi
