#!/bin/bash -l

# script location
BASH_DIR=$(dirname -- "$(readlink -f -- "$0")";)
# set conda env install dir
if [[ -z "${DOPPIOEXT_ENV}" ]]; then
  ENV_DIR=$BASH_DIR
else
  ENV_DIR="${DOPPIOEXT_ENV}"
fi
echo "## env dir for install: " $ENV_DIR

# set dir to wrap doppio_ext executables
if [[ -z "${DOPPIOEXT_EXE}" ]]; then
  EXE_DIR=$BASH_DIR
else
  EXE_DIR="${DOPPIOEXT_EXE}"
fi
echo "executables will be wrapped in : " $ENV_DIR

# find installed conda type
if which micromamba > /dev/null; then
    conda_type="micromamba"
    conda_create=$conda_type
elif which conda > /dev/null; then
    conda_type="conda"
    conda_create="${conda_type} env"
elif which mamba > /dev/null; then
    conda_type="mamba"
    conda_create=$conda_type
else
    echo "no conda/mamba installations found, exiting"
    exit 1
fi
echo "## conda type: " $conda_type

# SET PACKAGE ENVIRONMENTS
# check last argument, e.g. if it is "gpu"
eval ${1+"lastarg=\${$#"\}}
# create locscale env
# if [[ -d ./locscale ]]; then
#     rm -rf locscale
#     git clone -b Icknield https://gitlab.tudelft.nl/aj-lab/locscale.git
#     cd locscale
# else
#     cd locscale
#     git pull
# fi
if [[ $lastarg == "gpu" ]]; then
    yes | $conda_create create -f locscale_environment_gpu.yml -p $ENV_DIR/doppio-$conda_type/envs/gpu_locscale
    yes | $conda_type remove -p $ENV_DIR/doppio-$conda_type/envs/tempy_env --all
    yes | $conda_create create -f tempy_env_gpu.yml -p $ENV_DIR/doppio-$conda_type/envs/tempy_env

else
    yes | $conda_create create -f locscale_environment_cpu.yml -p $ENV_DIR/doppio-$conda_type/envs/cpu_locscale
    yes | $conda_type remove -p $ENV_DIR/doppio-$conda_type/envs/tempy_env --all
    yes | $conda_create create -f tempy_env.yml -p $ENV_DIR/doppio-$conda_type/envs/tempy_env

fi
# cd ..

# # create modelangelo env
grep_env="$($conda_type env list | grep " model_angelo ")"
if [[ ! $grep_env ]]; then
    if [ ! -d ./model-angelo ]; then
        git clone https://github.com/3dem/model-angelo.git
        cd model-angelo
    else
        cd model-angelo
        git pull
    fi
    cp ../install_modelangelo.sh .
    echo "Installing Model-angelo"
    source install_modelangelo.sh
    if [[ -z "${CONDA_PREFIX}" ]]; then
        grep_env="$($conda_type env list | grep " model_angelo ")"
        model_angelo_conda_prefix=`echo $grep_env | cut -d' ' -f2`
    else
        model_angelo_conda_prefix="${CONDA_PREFIX}"
    fi
    $conda_type deactivate
    cd ..
else
    model_angelo_conda_prefix=`echo $grep_env | cut -d' ' -f2`
fi

# root path
# ENV_DIR=$(shell $conda_type info --base)

while read -r e flag efiles; do
    if [[ $flag == "install" ]]; then
        if { $conda_type env list | grep $e; } >/dev/null 2>&1 && [[ -d $ENV_DIR/doppio-$conda_type/envs/$e ]]; then
            if [[ $# -ge 1 && $1 == "create" ]]; then
                echo "## Recreating" $e
                yes | $conda_type remove -p $ENV_DIR/doppio-$conda_type/envs/$e --all
                yes | $conda_create create -f $e.yml -p $ENV_DIR/doppio-$conda_type/envs/$e
            else
                echo "## Pruning" $e
                $conda_create update -f $e.yml -p $ENV_DIR/doppio-$conda_type/envs/$e --prune
            fi
        else
            echo "## Creating" $e
            yes | $conda_create create -f $e.yml -p $ENV_DIR/doppio-$conda_type/envs/$e
        fi
    fi
done < $BASH_DIR/list_env_yml

# install TEMPy-REFF
wget https://gitlab.com/topf-lab/embo23/-/raw/main/embo23.zip
yes | unzip embo23.zip
cp -r embo23/tools/tempy-reff/tempy_reff $ENV_DIR/doppio-micromamba/envs/tempy_env/lib/python3.10/site-packages/
cp tempy_reff $ENV_DIR/doppio-micromamba/envs/tempy_env/bin/
#WRAP binaries
while read -r e flag efiles; do
    if [[ $e != "ext_base" || ${#efiles} > 1 ]] && [[ -d $ENV_DIR/doppio-$conda_type/envs/$e/bin ]]; then
        echo  "## Wrapping executables:" $e
        $conda_type run -p $ENV_DIR/doppio-$conda_type/envs/ext_base create-wrappers -t conda --bin-dir \
        $ENV_DIR/doppio-$conda_type/envs/$e/bin --dest-dir $EXE_DIR/conda_wrappers/$e \
        --conda-env-dir $ENV_DIR/doppio-$conda_type/envs/$e -f $efiles

    fi
done < $BASH_DIR/list_env_yml

# wrap model-angelo exe
$conda_type run -p $ENV_DIR/doppio-$conda_type/envs/ext_base create-wrappers -t conda --bin-dir \
        $model_angelo_conda_prefix/bin --dest-dir $EXE_DIR/conda_wrappers/model-angelo \
        --conda-env-dir $model_angelo_conda_prefix -f model_angelo

echo "conda_type="$conda_type > setup_doppioext.sh
echo "ENV_DIR="$ENV_DIR >> setup_doppioext.sh
echo "EXE_DIR="$EXE_DIR >> setup_doppioext.sh
echo "BASH_DIR="$BASH_DIR >> setup_doppioext.sh
# echo "model_angelo_conda_bin="$model_angelo_conda_prefix/bin >> setup_doppioext.sh
cat setup_doppioext.sh.in >> setup_doppioext.sh
