# package-ccpem2

## Name

Mamba based packaging and installation of ccpem2 (doppio)

## Description

This repo will hold the scripts to package and install ccpem version 2 with the Doppio interface. Multiple mamba environments with one or more external tools are set-up, and Doppio+pipeliner will be run in a separate environment. Currently, only the script to set-up external environment is available and other setup and install scripts will be added in due course.

Currently the following tools are installed:  
`TEMPy2.0`  
`EMDA`  
`RIBFIND2.0`  
`Servalcat`  
`CheckMySequence & findMySequence`  
`Locscale` (CPU/GPU)

## Installation

Setting up the environments requires `micromamba` or `miniconda` installed. We recommend using `micromamba` as it is faster and lightweight compared to `miniconda`. Please see **micromamba installation** instructions here: https://mamba.readthedocs.io/en/latest/micromamba-installation.html

For Linux, macOS, or Git Bash on Windows install with:

```
"${SHELL}" <(curl -L micro.mamba.pm/install.sh)
```

Shell initialization is required before activating a mamba or conda environment. With micromamba, you can initialize a bash shell by running:

```
micromamba shell init -s bash -p ~/micromamba
```

This writes to .bashrc and the changes are applied to future interactive shells.

As an option, set up install location using the environment variable `DOPPIOEXT_ENV`. e.g.

```
export DOPPIOEXT_ENV=/Users/em
```

The mamba envs are then stored under /Users/em/doppio-micromamba .

Similarly, the location where program executables are wrapped can be set using `DOPPIOEXT_EXE`.

Environments for external tools can then be setup by:

```
cd setup_ext_env
bash install_doppioext.sh
source setup_doppioext.sh
```

To clean up any existing environments and create new ones, use:

`bash install_doppioext.sh create` in the second step above.

For gpu support, use:

`bash install_doppioext.sh gpu`

To recreate existing environments with gpu support, use:
`bash install_doppioext.sh create gpu`

New conda environments can be added (in setup_ext_env folder) by

1. adding a yaml file.  
   **The file basename should match with the environment name**
2. including the environment name, "install" flag (to create env and wrap executables) and executable names in list_env_yml file.  
   e.g. `checkmysequence_env install checkmysequence`. `checkmysequence_env` is the environment name and `checkmysequence` is the executable to be wrapped and added to the system PATH.

   The flag "wrap" instead of "install" will skip environment creation or update, and just wrap the executable. This is used only in specific cases that needs custom install options.

## Usage

Sourcing `setup_doppioext.sh` wraps up the executables and adds them to system PATH.  
It is useful to add this to .bashrc or .profile, so they are set when a new shell is spawned.

## License

This project will be released under the terms of the CCP-EM academic and commercial software licences.
